Hard Rock opened its doors in Siem Reap Angkor and established itself as one of the most popular restaurants burger bar with live music offering a great range of food and drinks, such as the Original Legendary ® Burger and Hurricane cocktail. We are playing music every night.

Address: King's Road Angkor, Street 7 Makara, Old Market Bridge, Siem Reap, 17252, Cambodia

Phone: +855 63 963 964

Website: https://www.hardrockcafe.com/location/angkor